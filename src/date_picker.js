import React from "react";
import DatePicker from "react-datepicker";
import {Modal, Button} from "react-bootstrap"
import './modal_window.css'
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';


export default class ModalWindow extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      show:false
    }
  }
  
     handleClose = () => {
       this.setState({
         show:false
       })
     };
     handleShow = () => {
      this.setState({
        show:true
      })
    };
  
    render(){
      return (
      <React.Fragment>
        <Button variant="primary" onClick={this.handleShow}>
          Modifier la Date
        </Button>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>EDITER LA DATE</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ChangeDate startDate={this.props.date} onChange={this.props.onChange}/>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              FERMER
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }}

class ChangeDate extends React.Component{
    constructor(props){
      super(props)
      this.state = {
        date:this.props.startDate
      }
    }

    handleOnchange(date,e){
      this.setState({
        date:date
      })
      this.props.onChange(date)
    }

   render(){
     return (
      <DatePicker selected={this.state.date} 
      onChange={(date,e)=>this.handleOnchange(date,e)} 
      dateFormat="dd/MM/yyyy"
      showTimeInput
      />
        )
      }
}