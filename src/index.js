import ReactDOM from 'react-dom'
import React from 'react'
import {JOUR,MOIS,DIVISION} from './variable_clock.js'
import ModalWindow from "./date_picker.js"
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";

class PartieJour extends React.Component{
   
    render(){
        return(
            <div className='datedays'>
                <h1>{this.props.situation}</h1>
            </div>
        )
    }
}

class Days extends React.Component{
    render(){
        return(
            <div className='days'>
                <h1>{this.props.jour_semaine.toUpperCase()}</h1>
            </div>
        )
    }
}

class Time extends React.Component{
    render(){
        console.log(this.props.minute.toString().padStart(2,0))
        const heure = this.props.heure.toString().length === 2 ? this.props.heure : this.props.heure.toString().padStart(2,0) 
        const minute = this.props.minute.toString().length === 2 ? this.props.minute : this.props.minute.toString().padStart(2,0) 
        return(
            <div className='time'>
                <h1>{heure + ': '+ minute}</h1>
            </div>
        )
    }
}

class DateDays extends React.Component{
    render(){
        console.log((this.props.jour).toString().length)
        const jour = this.props.jour.toString().length === 2 ? this.props.jour : this.props.jour.toString().padStart(2,0)
        return(
            <div className='datedays'>
                <h1>{jour +' '+ this.props.mois + ' ' + this.props.annee}</h1>
            </div>
        )
    }
}


class Clock extends React.Component{
    constructor(props){
        super(props);
        const date = new Date();
        this.state={
            par:1000,
            date:date,
            divisionJour:this.quelDivision(date.getHours()),
            seconde: date.getSeconds(),
            minute:date.getMinutes(),
            heure:date.getHours(),
            jour_semaine:date.getDay(),
            jour:date.getDate(),
            mois:date.getMonth(),
            annee:date.getFullYear(),
        }
        this.timer = null
    }
    componentDidMount(){
        this.timer = window.setInterval(this.getTime,this.state.par)
    }
    componentWillUnmount(){
        window.clearInterval(this.timer)
    }
    getTime =()=>{
        let date = this.state.date
        date.setSeconds(date.getSeconds() + 1)
        this.setState({
            date:date
        })
        if(this.state.minute !== date.getMinutes()){
            this.setState({
                minute:date.getMinutes()
            })
        }
        if(this.state.heure !== date.getHours){
            this.setState({
                heure:date.getHours()
            })
        }
        if(this.state.jour !== date.getDate()){
            this.setState({
                jour:date.getDate()
            })
        }
        if(this.state.jour_semaine !== date.getDay()){
            this.setState({
                jour_semaine: date.getDay
            })
        }
        if(this.state.mois !== date.getMonth()){
            this.setState({
                mois:date.getMonth()
            })
        }
        if(this.state.annee !== date.getFullYear()){
            this.setState({
                annee:date.getFullYear()
            })
        }
    }

    
    quelJour=(numberDays) =>{
        return JOUR[numberDays]
    }
    quelMois=(numberDays)=> {
        return MOIS[numberDays]
    }
    quelDivision = (numberDays) =>{
        return DIVISION[numberDays]
    }
    handleChangeDate = (date)=>{
        this.setState({
            date:date,
            divisionJour:this.quelDivision(date.getHours()),
            seconde: date.getSeconds(),
            minute:date.getMinutes(),
            heure:date.getHours(),
            jour_semaine:date.getDay(),
            jour:date.getDate(),
            mois:date.getMonth(),
            annee:date.getFullYear(),
        })
    }
    render(){
        return(
            <div className="shadow-lg p-5 bg-body rounded-3 w-100 h-100">
                <div className='clock d-flex flex-column justify-content-center p-5'>
                    <Days jour_semaine={this.quelJour(this.state.jour_semaine)}/>
                    <PartieJour situation = {this.state.divisionJour}/>
                    <Time heure={this.state.heure} minute={this.state.minute}/>
                    <DateDays jour= {this.state.jour} mois={this.quelMois(this.state.mois)} annee={this.state.annee}/>
                    <ModalWindow date={this.state.date} onChange={this.handleChangeDate}/>
                </div>
            </div>
        )
    }
}


ReactDOM.render(<Clock />, document.getElementById("root"));