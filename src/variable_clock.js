let JOUR = ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi']
let MOIS = ['Janvier','Fevrier','mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']
let DIVISION ={
    1:'Nuit',
    2:'Nuit',
    3:'Nuit',
    4:'Nuit',
    5:'Nuit',
    6:'Matin',
    7:'Matin',
    8:'Matin',
    9:'Matin',
    10:'Matin',
    11:'Matin',
    12:'Apres-Midi',
    13:'Apres-Midi',
    14:'Apres-Midi',
    15:'Apres-Midi',
    16:'Apres-Midi',
    17:'Apres-Midi',
    18:'Soir',
    19:'Nuit',
    20:'Nuit',
    21:'Nuit',
    22:'Nuit',
    23:'Nuit',
    24:'Nuit',
}
export {JOUR, MOIS, DIVISION}